from matplotlib.widgets import Slider, RadioButtons
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import math
import ax12

def main():
    myRobot = Draw_Robot()


class Draw_Robot():

    def __init__(self):
        # --- Variable definitions ---
        # (MOST IMPORTANT! THIS IS REQUIRED TO CONFIGURE YOUR ROBOT ARM)
        SEGMENTS = int(4)  # AMOUNT of Segments (4)
        # Could probably make these lists instead of arrays
        self.l = np.array([0, 80, 80, 95])  # Actual measurements of segment length in mm
        self.hor = np.array([0] * SEGMENTS, dtype=float)  # Horizontal coordinate
        self.ver = np.array([0] * SEGMENTS, dtype=float)  # Vertical coordinate
        self.x = np.array([0] * SEGMENTS, dtype=float)  # X axis components
        self.y = np.array([0] * SEGMENTS, dtype=float)  # Y axis components
        self.a = np.array([np.pi] * SEGMENTS, dtype=float)  # Angle for the link, reference is previous link
        self.gripper_angle = np.array([0, -45, -90, 45, 90])  # Preselected gripper angles (grijper/klauw angle)
        self.current_gripper = 1  # gripper angle selection (gripper 1 = 45 degree angle)
        self.tw = 65.0  # W axis position depth
        self.tz = 50.0  # Z axis starting position height
        self.l12 = 0.0  # Hypotenuse between a1 & a2 (distance/connection between a1 & a2)
        self.a12 = 0.0  # Inscribed angle between Hypotenuse
        self.fig = plt.figure("Kinematics Robot Arm Simulator", figsize=(8, 6))  # Create frame
        self.ax = plt.axes([0.05, 0.2, 0.90, .75], projection='3d')  # 3D AX Panel
        self.axe = plt.axes([0.25, 0.85, 0.001, .001])  # Panel for error message

        # --- Variables for AX12 Servo Movement ---
        self.s = ax12.Ax12() # AX12 Class to control the AX12 servo's
        self.conv_angles = np.array([0, 0, 0, 0]) # Converted Angles Array
        self.servo_speed = 150 # Servo Movement Speed
        self.from_id = 100 # Where the Servo id's should start from
        # Min and Max Degree turn (from 0 to 300)
        self.servo_deg_limit = [0, 300]
        # Min and Max Fixed Servo Positions
        self.servo_pos_limit = [0, 1023]
        # The Min and Max Defaults are based on this reference: http://support.robotis.com/en/product/actuator/dynamixel/ax_series/dxl_ax_actuator.htm
        self.servo_limits = [ [0, 1000], [500, 900], [800, 1000], [250, 900] ] # Your own AX12 Servo Motor Position Limits (order: Min, Max for each servo)

        # --- Draw Widgets ---
        # Draw a0 slider panel
        axxval = plt.axes([0.35, 0.1, 0.45, 0.03])
        axxval.set_title('Rotations in Degrees' + u"\u00b0", fontsize=10)
        # 140 is the MAX Degree turn for the Servo that handles the X movement (160 - 20 = 140)
        a0_val = Slider(axxval, 'Rotation a0 (X)', 20, 160, valinit=20)

        # Draw tw slider panel
        axyval = plt.axes([0.35, 0.0575, 0.45, 0.03])
        tw_val = Slider(axyval, 'Extension (W)', 35, 280, valinit=65)

        # Draw xval slider panel
        axzval = plt.axes([0.35, 0.015, 0.45, 0.03])
        z_val = Slider(axzval, 'Height (Z)', 0, 280, valinit=50)

        # Radio current_gripper_set buttons
        rax = plt.axes([0.05, 0.02, 0.12, 0.12])  # , axisbg=axcolor)
        rax.set_title('Gripper Angle', fontsize=12)
        set_gripper = RadioButtons(rax, ('0' + u"\u00b0", '45' + u"\u00b0", '90' + u"\u00b0"), active=1)

        self.display_error()  # Draw the error and hide it
        self.draw_robot()  # Draw function to draw robot

        # --- Widget Event Handlers (updates positions) ---

        def update_a0_val(val):  # Slider a0 theta Event
            self.a[0] = np.deg2rad(val)
            self.draw_robot()

        # Update self.a0 Value
        a0_val.on_changed(update_a0_val) 

        def update_tw_val(val):  # W Slider Event (Extension)
            self.tw = val
            self.draw_robot()

        # Update self.tw value
        tw_val.on_changed(update_tw_val)

        def update_z_val(val):  # Z Slider event (Height)
            self.tz = val
            self.draw_robot()

        # Update self.tz value
        z_val.on_changed(update_z_val)

        def set_gripper_angle(label):  # Simulation Radio Button Event
            if label == ('0' + u"\u00b0"):
                self.current_gripper = 0
            if label == ('45' + u"\u00b0"):
                self.current_gripper = 1
            if label == ('90' + u"\u00b0"):
                self.current_gripper = 2
            self.draw_robot()

        set_gripper.on_clicked(set_gripper_angle)

        plt.show()
        # End of constructor

    # --- Class Fuctions ---
    def display_error(self):
        self.axe.set_visible(False)
        self.axe.set_yticks([])
        self.axe.set_xticks([])
        self.axe.set_navigate(False)
        self.axe.text(0, 0, 'Arm Can Not Reach the Target!', style='oblique',
                      bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10}, size=20, va='baseline')

    def calc_p2(self):  # Calculates position 2
        self.hor[3] = self.tw
        self.ver[3] = self.tz
        self.hor[2] = self.tw - np.cos(np.radians(self.gripper_angle[self.current_gripper])) * self.l[3]
        self.ver[2] = self.tz - np.sin(np.radians(self.gripper_angle[self.current_gripper])) * self.l[3]
        self.l12 = np.sqrt(np.square(self.hor[2]) + np.square(self.ver[2]))

    def calc_p1(self):  # Calculates position 1
        self.a12 = np.arctan2(self.ver[2], self.hor[2])  # return the appropriate quadrant
        self.a[1] = np.arccos((np.square(self.l[1]) + np.square(self.l12) - np.square(self.l[2]))
                              / (2 * self.l[1] * self.l12)) + self.a12

        self.hor[1] = np.cos(self.a[1]) * self.l[1]
        self.ver[1] = np.sin(self.a[1]) * self.l[1]

    def calc_x_y(self):  # Calculates X and Y coordinates
        for i in range(len(self.x)):
            self.x[i] = self.hor[i] * np.cos(self.a[0])
            self.y[i] = self.hor[i] * np.sin(self.a[0])

    def set_positions(self):  # Gets the x,y,z values for the line.
        # Convert arrays to lists for drawing the line
        xs = np.array(self.x).tolist()  # = (self.ver[0], self.ver[1], self.ver[2], self.ver[3])
        ys = np.array(self.y).tolist()
        zs = np.array(self.ver).tolist()
        self.ax.cla()  # clear current axis

        # draw new lines,  two lines for "fancy" looks
        self.ax.plot(xs, ys, zs, 'o-', markersize=20,
                     markerfacecolor="orange", linewidth=8, color="blue")
        self.ax.plot(xs, ys, zs, 'o-', markersize=4,
                     markerfacecolor="blue", linewidth=1, color="silver")

    def set_ax(self):  # AX panel set up
        self.ax.set_xlim3d(-200, 200)
        self.ax.set_ylim3d(-200, 200)
        self.ax.set_zlim3d(-5, 200)
        self.ax.set_xlabel('X axis')
        self.ax.set_ylabel('Y axis')
        self.ax.set_zlabel('Z axis')

        for j in self.ax.get_xticklabels() + self.ax.get_yticklabels():  # Hide ticks
            j.set_visible(False)

        self.ax.set_axisbelow(True)  # Send grid lines to the background

    def get_angles(self):  # Get all of the motor angles see diagram
        self.a[2] = np.arctan((self.ver[2] - self.ver[1]) / (self.hor[2] - self.hor[1])) - self.a[1]
        self.a[3] = np.deg2rad(self.gripper_angle[self.current_gripper]) - self.a[1] - self.a[2]
        angles = np.array(self.a).tolist()

        return angles

    def control_ax(self):
        # Converting All Angles to Positive Angles
        self.convert_angles()

        # Converting the Angles to actual Servo Positions and send it to the Servo
        i=0
        print("Moving servo's..")
        for a in self.conv_angles:
            servo_id = self.from_id + i # Generate the Servo id's you use
            servo_pos = a * ( self.servo_pos_limit[1] / self.servo_deg_limit[1] ) # Calculate Servo Position = given_angle_in_degrees * ( servo_pos_limit / servo_degree_limit )
            self.s.moveSpeed(servo_id, servo_pos, self.servo_speed) # Move current Servo to the given Calculated Position
            i+=1

        # Output Servo Positions
        #print(self.conv_angles)

    def to_positive_angle(self, angle):
        angle = math.fmod(angle, 300)
        if angle < 0: 
            angle += 300

        return angle

    def convert_angles(self):
        i=0
        for a in self.get_angles():
            self.conv_angles[i] = self.to_positive_angle(np.around(np.rad2deg(a), 2)) # Only positive Degree Angles are allowed for the servo motors
            i+=1

    def draw_robot(self):  # Draw and update the 3D panel
        self.calc_p2()
        self.calc_p1()
        self.calc_x_y()

        if self.l12 < (self.l[1] + self.l[2]):  # Check boundaries
            self.axe.set_visible(False)  # Turn off error message panel
            self.set_positions()
            self.set_ax()

            # Control AX12 Servo's
            self.control_ax()

        else: 
            # Error, cannot reach target
            self.axe.set_visible(True)  # Display error message panel

        plt.draw()


if __name__ == '__main__':
    main()
