# 3D Kinematics for AX12 Servo's

A script that creates the ability to configure your own Kinematics for your Robot Arm in a 3D simulation.
It is compatible with AX12 Servo's so you can control your own Robot Arm.

## Getting Started

Follow the following instructions to setup the script.

### Prerequisites

The packages you need to run the software:
1. MatPlotLib
2. NumPy
3. PySerial
4. RPi.GPIO

Execute the following commands:

```
sudo apt-get install python3-matplotlib
sudo python3 -m pip install pyserial
```

and if NOT installed beforehand:

```
sudo apt-get install numpy
```

Note: Place the AX12 Library in the same folder with all your other Python scripts.

## Run script
It is recommended to run this script with Python3.
```
python3 main.py
```


## Build with

* [MatPlotLib](https://matplotlib.org/) - The Plot Library I've used
* [AX12 Library (requires: PySerial, RPi.GPIO)](https://github.com/aimlabmu/dynamixel-rpi/blob/master/ax12/ax12.py) - AX12 Library in Python 3

## Handy to know
[How to Convert Radians to Degrees](https://socratic.org/questions/how-do-you-convert-negative-radians-to-degrees)



## Credits
* **Phil Williammeee** - *Initial work* - [Raspberry Pi Forum Topic](https://www.raspberrypi.org/forums/viewtopic.php?f=41&t=34300&p=291674#p291674)


## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments
All credits go to the Authors below who's code helped me to create this script.
I found the code on a forum which was never updated and unfortunately, not uploaded to GitHub as a respository. So it felt as my task to do this for you guys because I've searched the whole internet for a script like this but this is the only good one, so if you want to contribute to this project, message me!